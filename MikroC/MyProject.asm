
_main:

;MyProject.c,38 :: 		void main() {
;MyProject.c,39 :: 		INTCON = 0x80; // set GIE
	MOVLW      128
	MOVWF      INTCON+0
;MyProject.c,40 :: 		portSetup();
	CALL       _portSetup+0
;MyProject.c,41 :: 		lcdSetup();
	CALL       _lcdSetup+0
;MyProject.c,42 :: 		while (1) {
L_main0:
;MyProject.c,43 :: 		if (checkSensor(FRONTS)) {
	CLRF       FARG_checkSensor_type+0
	CALL       _checkSensor+0
	MOVF       R0+0, 0
	IORWF      R0+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main2
;MyProject.c,44 :: 		if (checkSensor(RIGHTS)) {
	MOVLW      1
	MOVWF      FARG_checkSensor_type+0
	CALL       _checkSensor+0
	MOVF       R0+0, 0
	IORWF      R0+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main3
;MyProject.c,45 :: 		if (checkSensor(LEFTS)) {
	MOVLW      2
	MOVWF      FARG_checkSensor_type+0
	CALL       _checkSensor+0
	MOVF       R0+0, 0
	IORWF      R0+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main4
;MyProject.c,46 :: 		turn(AROUND);
	MOVLW      2
	MOVWF      FARG_turn_type+0
	CALL       _turn+0
;MyProject.c,47 :: 		} else {
	GOTO       L_main5
L_main4:
;MyProject.c,48 :: 		turn(LEFT);
	MOVLW      1
	MOVWF      FARG_turn_type+0
	CALL       _turn+0
;MyProject.c,49 :: 		}
L_main5:
;MyProject.c,50 :: 		} else {
	GOTO       L_main6
L_main3:
;MyProject.c,51 :: 		turn(RIGHT);
	CLRF       FARG_turn_type+0
	CALL       _turn+0
;MyProject.c,52 :: 		}
L_main6:
;MyProject.c,53 :: 		} else {
	GOTO       L_main7
L_main2:
;MyProject.c,54 :: 		moveForward();
	CALL       _moveForward+0
;MyProject.c,55 :: 		}
L_main7:
;MyProject.c,56 :: 		}
	GOTO       L_main0
;MyProject.c,57 :: 		}
L_end_main:
	GOTO       $+0
; end of _main

_portSetup:

;MyProject.c,58 :: 		void portSetup(void) {
;MyProject.c,59 :: 		TRISD = 0x00;
	CLRF       TRISD+0
;MyProject.c,60 :: 		TRISC = 0x2A;
	MOVLW      42
	MOVWF      TRISC+0
;MyProject.c,61 :: 		}
L_end_portSetup:
	RETURN
; end of _portSetup

_lcdSetup:

;MyProject.c,62 :: 		void lcdSetup(void) {
;MyProject.c,63 :: 		Lcd_Init();                 // Initialize LCD
	CALL       _Lcd_Init+0
;MyProject.c,64 :: 		Lcd_Cmd(_LCD_CLEAR);       // Clear display
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,65 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);  // Cursor off
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,66 :: 		}
L_end_lcdSetup:
	RETURN
; end of _lcdSetup

_convertNumToText:

;MyProject.c,67 :: 		void convertNumToText(int x) {  //put x in num[10]
;MyProject.c,68 :: 		int i = 0, length;
	CLRF       convertNumToText_i_L0+0
	CLRF       convertNumToText_i_L0+1
;MyProject.c,69 :: 		while (x) {
L_convertNumToText8:
	MOVF       FARG_convertNumToText_x+0, 0
	IORWF      FARG_convertNumToText_x+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_convertNumToText9
;MyProject.c,70 :: 		num[i] = (x % 10) + '0';
	MOVF       convertNumToText_i_L0+0, 0
	ADDLW      _num+0
	MOVWF      FLOC__convertNumToText+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       FARG_convertNumToText_x+0, 0
	MOVWF      R0+0
	MOVF       FARG_convertNumToText_x+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      48
	ADDWF      R0+0, 1
	MOVF       FLOC__convertNumToText+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;MyProject.c,71 :: 		x /= 10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       FARG_convertNumToText_x+0, 0
	MOVWF      R0+0
	MOVF       FARG_convertNumToText_x+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	MOVWF      FARG_convertNumToText_x+0
	MOVF       R0+1, 0
	MOVWF      FARG_convertNumToText_x+1
;MyProject.c,72 :: 		i++;
	INCF       convertNumToText_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       convertNumToText_i_L0+1, 1
;MyProject.c,73 :: 		}
	GOTO       L_convertNumToText8
L_convertNumToText9:
;MyProject.c,74 :: 		length = i;
	MOVF       convertNumToText_i_L0+0, 0
	MOVWF      convertNumToText_length_L0+0
	MOVF       convertNumToText_i_L0+1, 0
	MOVWF      convertNumToText_length_L0+1
;MyProject.c,75 :: 		for (i = 0; i < length / 2; ++i) {
	CLRF       convertNumToText_i_L0+0
	CLRF       convertNumToText_i_L0+1
L_convertNumToText10:
	MOVF       convertNumToText_length_L0+0, 0
	MOVWF      R1+0
	MOVF       convertNumToText_length_L0+1, 0
	MOVWF      R1+1
	RRF        R1+1, 1
	RRF        R1+0, 1
	BCF        R1+1, 7
	BTFSC      R1+1, 6
	BSF        R1+1, 7
	BTFSS      R1+1, 7
	GOTO       L__convertNumToText34
	BTFSS      STATUS+0, 0
	GOTO       L__convertNumToText34
	INCF       R1+0, 1
	BTFSC      STATUS+0, 2
	INCF       R1+1, 1
L__convertNumToText34:
	MOVLW      128
	XORWF      convertNumToText_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	XORWF      R1+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__convertNumToText35
	MOVF       R1+0, 0
	SUBWF      convertNumToText_i_L0+0, 0
L__convertNumToText35:
	BTFSC      STATUS+0, 0
	GOTO       L_convertNumToText11
;MyProject.c,76 :: 		char tmp = num[i];
	MOVF       convertNumToText_i_L0+0, 0
	ADDLW      _num+0
	MOVWF      R2+0
	MOVF       R2+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      convertNumToText_tmp_L1+0
;MyProject.c,77 :: 		num[i] = num[length - i - 1];
	MOVF       convertNumToText_i_L0+0, 0
	SUBWF      convertNumToText_length_L0+0, 0
	MOVWF      R0+0
	MOVF       convertNumToText_i_L0+1, 0
	BTFSS      STATUS+0, 0
	ADDLW      1
	SUBWF      convertNumToText_length_L0+1, 0
	MOVWF      R0+1
	MOVLW      1
	SUBWF      R0+0, 1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVF       R0+0, 0
	ADDLW      _num+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R2+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;MyProject.c,78 :: 		num[length - i - 1] = tmp;
	MOVF       convertNumToText_i_L0+0, 0
	SUBWF      convertNumToText_length_L0+0, 0
	MOVWF      R0+0
	MOVF       convertNumToText_i_L0+1, 0
	BTFSS      STATUS+0, 0
	ADDLW      1
	SUBWF      convertNumToText_length_L0+1, 0
	MOVWF      R0+1
	MOVLW      1
	SUBWF      R0+0, 1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVF       R0+0, 0
	ADDLW      _num+0
	MOVWF      FSR
	MOVF       convertNumToText_tmp_L1+0, 0
	MOVWF      INDF+0
;MyProject.c,75 :: 		for (i = 0; i < length / 2; ++i) {
	INCF       convertNumToText_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       convertNumToText_i_L0+1, 1
;MyProject.c,79 :: 		}
	GOTO       L_convertNumToText10
L_convertNumToText11:
;MyProject.c,80 :: 		}
L_end_convertNumToText:
	RETURN
; end of _convertNumToText

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;MyProject.c,81 :: 		void interrupt() {  //used for delay functions
;MyProject.c,82 :: 		TMR0 = resetTMR0;
	MOVF       _resetTMR0+0, 0
	MOVWF      TMR0+0
;MyProject.c,83 :: 		delayCntr++;
	INCF       _delayCntr+0, 1
	BTFSC      STATUS+0, 2
	INCF       _delayCntr+1, 1
;MyProject.c,84 :: 		INTCON = INTCON & 0xFB;
	MOVLW      251
	ANDWF      INTCON+0, 1
;MyProject.c,85 :: 		}
L_end_interrupt:
L__interrupt37:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_moveForward:

;MyProject.c,86 :: 		void moveForward() {
;MyProject.c,87 :: 		PORTD = 0xA0;
	MOVLW      160
	MOVWF      PORTD+0
;MyProject.c,88 :: 		Lcd_Out(1, 1, moveForwardTxt);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _moveForwardTxt+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,89 :: 		}
L_end_moveForward:
	RETURN
; end of _moveForward

_delayFor_ms:

;MyProject.c,90 :: 		void delayFor_ms(unsigned int d) {
;MyProject.c,91 :: 		delayCntr = 0;
	CLRF       _delayCntr+0
	CLRF       _delayCntr+1
;MyProject.c,92 :: 		OPTION_REG = 0x87; //256 prescaler
	MOVLW      135
	MOVWF      OPTION_REG+0
;MyProject.c,93 :: 		resetTMR0 = 247;   //overflows every 1ms
	MOVLW      247
	MOVWF      _resetTMR0+0
	CLRF       _resetTMR0+1
;MyProject.c,94 :: 		TMR0 = resetTMR0;
	MOVLW      247
	MOVWF      TMR0+0
;MyProject.c,95 :: 		INTCON = INTCON | 0x20; //Enable TMRO Overflow interrupt
	BSF        INTCON+0, 5
;MyProject.c,96 :: 		while (delayCntr < d);
L_delayFor_ms13:
	MOVF       FARG_delayFor_ms_d+1, 0
	SUBWF      _delayCntr+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__delayFor_ms40
	MOVF       FARG_delayFor_ms_d+0, 0
	SUBWF      _delayCntr+0, 0
L__delayFor_ms40:
	BTFSC      STATUS+0, 0
	GOTO       L_delayFor_ms14
	GOTO       L_delayFor_ms13
L_delayFor_ms14:
;MyProject.c,97 :: 		INTCON = INTCON & 0xDF;// Disable TMR0 Overflow interrupt
	MOVLW      223
	ANDWF      INTCON+0, 1
;MyProject.c,98 :: 		}
L_end_delayFor_ms:
	RETURN
; end of _delayFor_ms

_delayFor_10us:

;MyProject.c,99 :: 		void delayFor_10us(unsigned int d) {
;MyProject.c,100 :: 		delayCntr = 0;
	CLRF       _delayCntr+0
	CLRF       _delayCntr+1
;MyProject.c,101 :: 		OPTION_REG = 0x80; //2 prescaler
	MOVLW      128
	MOVWF      OPTION_REG+0
;MyProject.c,102 :: 		resetTMR0 = 245;  //overflows every 10us
	MOVLW      245
	MOVWF      _resetTMR0+0
	CLRF       _resetTMR0+1
;MyProject.c,103 :: 		TMR0 = resetTMR0;
	MOVLW      245
	MOVWF      TMR0+0
;MyProject.c,104 :: 		INTCON = INTCON | 0x20; //Enable TMRO Overflow interrupt
	BSF        INTCON+0, 5
;MyProject.c,105 :: 		while (delayCntr < d);
L_delayFor_10us15:
	MOVF       FARG_delayFor_10us_d+1, 0
	SUBWF      _delayCntr+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__delayFor_10us42
	MOVF       FARG_delayFor_10us_d+0, 0
	SUBWF      _delayCntr+0, 0
L__delayFor_10us42:
	BTFSC      STATUS+0, 0
	GOTO       L_delayFor_10us16
	GOTO       L_delayFor_10us15
L_delayFor_10us16:
;MyProject.c,106 :: 		INTCON = INTCON & 0xDF;// Disable TMR0 Overflow interrupt
	MOVLW      223
	ANDWF      INTCON+0, 1
;MyProject.c,107 :: 		}
L_end_delayFor_10us:
	RETURN
; end of _delayFor_10us

_findDis:

;MyProject.c,108 :: 		unsigned int findDis(int trigPin, int echoPin) {
;MyProject.c,110 :: 		delayFor_ms(500);
	MOVLW      244
	MOVWF      FARG_delayFor_ms_d+0
	MOVLW      1
	MOVWF      FARG_delayFor_ms_d+1
	CALL       _delayFor_ms+0
;MyProject.c,111 :: 		PORTC = 0;
	CLRF       PORTC+0
;MyProject.c,112 :: 		TMR1H = TMR1L = 0;
	CLRF       TMR1L+0
	MOVF       TMR1L+0, 0
	MOVWF      TMR1H+0
;MyProject.c,113 :: 		delayFor_10us(5);
	MOVLW      5
	MOVWF      FARG_delayFor_10us_d+0
	MOVLW      0
	MOVWF      FARG_delayFor_10us_d+1
	CALL       _delayFor_10us+0
;MyProject.c,114 :: 		PORTC = trigPin;  //start trigger signal 50us pulse
	MOVF       FARG_findDis_trigPin+0, 0
	MOVWF      PORTC+0
;MyProject.c,115 :: 		delayFor_10us(5);
	MOVLW      5
	MOVWF      FARG_delayFor_10us_d+0
	MOVLW      0
	MOVWF      FARG_delayFor_10us_d+1
	CALL       _delayFor_10us+0
;MyProject.c,116 :: 		PORTC = 0x00;
	CLRF       PORTC+0
;MyProject.c,117 :: 		while ((PORTC & echoPin) == 0);  //wait for echo pulse
L_findDis17:
	MOVF       FARG_findDis_echoPin+0, 0
	ANDWF      PORTC+0, 0
	MOVWF      R1+0
	MOVLW      0
	ANDWF      FARG_findDis_echoPin+1, 0
	MOVWF      R1+1
	MOVLW      0
	XORWF      R1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__findDis44
	MOVLW      0
	XORWF      R1+0, 0
L__findDis44:
	BTFSS      STATUS+0, 2
	GOTO       L_findDis18
	GOTO       L_findDis17
L_findDis18:
;MyProject.c,118 :: 		T1CON = 0x01;
	MOVLW      1
	MOVWF      T1CON+0
;MyProject.c,119 :: 		while (PORTC & echoPin);
L_findDis19:
	MOVF       FARG_findDis_echoPin+0, 0
	ANDWF      PORTC+0, 0
	MOVWF      R0+0
	MOVLW      0
	ANDWF      FARG_findDis_echoPin+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	IORWF      R0+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_findDis20
	GOTO       L_findDis19
L_findDis20:
;MyProject.c,120 :: 		T1CON = 0x00;
	CLRF       T1CON+0
;MyProject.c,121 :: 		dis = (TMR1L | (TMR1H << 8));
	MOVF       TMR1H+0, 0
	MOVWF      R0+1
	CLRF       R0+0
	MOVF       TMR1L+0, 0
	IORWF      R0+0, 1
	MOVLW      0
	IORWF      R0+1, 1
;MyProject.c,122 :: 		dis = (dis +116 - 1)/ 116;  //dis = ceil(dis/116)
	MOVLW      116
	ADDWF      R0+0, 1
	BTFSC      STATUS+0, 0
	INCF       R0+1, 1
	MOVLW      1
	SUBWF      R0+0, 1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVLW      116
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
;MyProject.c,123 :: 		return dis;
;MyProject.c,124 :: 		}
L_end_findDis:
	RETURN
; end of _findDis

_checkSensor:

;MyProject.c,125 :: 		unsigned int checkSensor(char type) {
;MyProject.c,127 :: 		if (type == FRONTS) {
	MOVF       FARG_checkSensor_type+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_checkSensor21
;MyProject.c,128 :: 		dis = findDis(1, 2);
	MOVLW      1
	MOVWF      FARG_findDis_trigPin+0
	MOVLW      0
	MOVWF      FARG_findDis_trigPin+1
	MOVLW      2
	MOVWF      FARG_findDis_echoPin+0
	MOVLW      0
	MOVWF      FARG_findDis_echoPin+1
	CALL       _findDis+0
	MOVF       R0+0, 0
	MOVWF      checkSensor_dis_L0+0
	MOVF       R0+1, 0
	MOVWF      checkSensor_dis_L0+1
;MyProject.c,129 :: 		convertNumToText(dis);
	MOVF       R0+0, 0
	MOVWF      FARG_convertNumToText_x+0
	MOVF       R0+1, 0
	MOVWF      FARG_convertNumToText_x+1
	CALL       _convertNumToText+0
;MyProject.c,130 :: 		Lcd_Out(2, 1, frontSensorTxt);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _frontSensorTxt+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,131 :: 		Lcd_Out(2, 15, num);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      15
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _num+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,132 :: 		Lcd_Out(2,18,"cm");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      18
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,133 :: 		} else if (type == RIGHTS) {
	GOTO       L_checkSensor22
L_checkSensor21:
	MOVF       FARG_checkSensor_type+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_checkSensor23
;MyProject.c,134 :: 		dis = findDis(4, 8);
	MOVLW      4
	MOVWF      FARG_findDis_trigPin+0
	MOVLW      0
	MOVWF      FARG_findDis_trigPin+1
	MOVLW      8
	MOVWF      FARG_findDis_echoPin+0
	MOVLW      0
	MOVWF      FARG_findDis_echoPin+1
	CALL       _findDis+0
	MOVF       R0+0, 0
	MOVWF      checkSensor_dis_L0+0
	MOVF       R0+1, 0
	MOVWF      checkSensor_dis_L0+1
;MyProject.c,135 :: 		}  else {
	GOTO       L_checkSensor24
L_checkSensor23:
;MyProject.c,136 :: 		dis = findDis(16, 32);
	MOVLW      16
	MOVWF      FARG_findDis_trigPin+0
	MOVLW      0
	MOVWF      FARG_findDis_trigPin+1
	MOVLW      32
	MOVWF      FARG_findDis_echoPin+0
	MOVLW      0
	MOVWF      FARG_findDis_echoPin+1
	CALL       _findDis+0
	MOVF       R0+0, 0
	MOVWF      checkSensor_dis_L0+0
	MOVF       R0+1, 0
	MOVWF      checkSensor_dis_L0+1
;MyProject.c,137 :: 		}
L_checkSensor24:
L_checkSensor22:
;MyProject.c,138 :: 		if (dis >= thresholdDis)return 0; //no obstacle detected
	MOVLW      128
	XORWF      checkSensor_dis_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	XORLW      0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__checkSensor46
	MOVLW      40
	SUBWF      checkSensor_dis_L0+0, 0
L__checkSensor46:
	BTFSS      STATUS+0, 0
	GOTO       L_checkSensor25
	CLRF       R0+0
	CLRF       R0+1
	GOTO       L_end_checkSensor
L_checkSensor25:
;MyProject.c,139 :: 		return 1;
	MOVLW      1
	MOVWF      R0+0
	MOVLW      0
	MOVWF      R0+1
;MyProject.c,140 :: 		}
L_end_checkSensor:
	RETURN
; end of _checkSensor

_turn:

;MyProject.c,141 :: 		void turn(char type) {
;MyProject.c,142 :: 		if (type == RIGHT) {
	MOVF       FARG_turn_type+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_turn26
;MyProject.c,143 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,144 :: 		Lcd_Out(1, 1, turnRightTxt);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _turnRightTxt+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,145 :: 		PORTD  =  0x80;  //stop right motor
	MOVLW      128
	MOVWF      PORTD+0
;MyProject.c,146 :: 		delayFor_ms(2000);
	MOVLW      208
	MOVWF      FARG_delayFor_ms_d+0
	MOVLW      7
	MOVWF      FARG_delayFor_ms_d+1
	CALL       _delayFor_ms+0
;MyProject.c,147 :: 		} else if (type == LEFT) {
	GOTO       L_turn27
L_turn26:
	MOVF       FARG_turn_type+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_turn28
;MyProject.c,148 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,149 :: 		Lcd_Out(1, 1, turnLeftTxt);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _turnLeftTxt+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,150 :: 		PORTD  = 0x20;  //stop left motor
	MOVLW      32
	MOVWF      PORTD+0
;MyProject.c,151 :: 		delayFor_ms(2000);
	MOVLW      208
	MOVWF      FARG_delayFor_ms_d+0
	MOVLW      7
	MOVWF      FARG_delayFor_ms_d+1
	CALL       _delayFor_ms+0
;MyProject.c,152 :: 		} else {
	GOTO       L_turn29
L_turn28:
;MyProject.c,153 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,154 :: 		Lcd_Out(1, 1, turn180Txt);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _turn180Txt+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,155 :: 		PORTD  =  0x80; //stop right motor, turn 180 degrees
	MOVLW      128
	MOVWF      PORTD+0
;MyProject.c,156 :: 		delayFor_ms(4000);
	MOVLW      160
	MOVWF      FARG_delayFor_ms_d+0
	MOVLW      15
	MOVWF      FARG_delayFor_ms_d+1
	CALL       _delayFor_ms+0
;MyProject.c,157 :: 		}
L_turn29:
L_turn27:
;MyProject.c,158 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,159 :: 		Lcd_Out(1, 1, doneTurngingTxt);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _doneTurngingTxt+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,160 :: 		delayFor_ms(500);
	MOVLW      244
	MOVWF      FARG_delayFor_ms_d+0
	MOVLW      1
	MOVWF      FARG_delayFor_ms_d+1
	CALL       _delayFor_ms+0
;MyProject.c,161 :: 		}
L_end_turn:
	RETURN
; end of _turn

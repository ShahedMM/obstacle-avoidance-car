// LCD module connections
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
// End LCD module connections
const char FRONTS = 0, RIGHTS = 1, LEFTS = 2;
const char RIGHT = 0, LEFT = 1, AROUND = 2;
const int thresholdDis = 40; //Obstacle detected if dis < 40
int delayCntr = 0;
int resetTMR0 = 0;
char turnRightTxt[] = "turning right";
char turnLeftTxt[] = "turning left";
char turn180Txt[] = "turning 180 degrees";
char moveForwardTxt[] = "moving forward";
char checkSensorTxt[] = "checking sensor";
char doneTurngingTxt[] = "done turning";
char frontSensorTxt[]="front sensor: ";
char num[10];
unsigned int checkSensor(char type);
void convertNumToText(int x);
void delayFor_ms(unsigned int d);
void delayFor_us(unsigned int d);
void portSetup(void);
void lcdSetup(void);
void moveForward();
void turn(char type);
void interrupt();
void main() {
  INTCON = 0x80; // set GIE
  portSetup();
  lcdSetup();
  while (1) {
    if (checkSensor(FRONTS)) {
      if (checkSensor(RIGHTS)) {
        if (checkSensor(LEFTS)) {
          turn(AROUND);
        } else {
          turn(LEFT);
        }
      } else {
        turn(RIGHT);
      }
    } else {
      moveForward();
    }
  }
}
void portSetup(void) {
  TRISD = 0x00;
  TRISC = 0x2A;
}
void lcdSetup(void) {
  Lcd_Init();                 // Initialize LCD
  Lcd_Cmd(_LCD_CLEAR);       // Clear display
  Lcd_Cmd(_LCD_CURSOR_OFF);  // Cursor off
}
void convertNumToText(int x) {  //put x in num[10]
  int i = 0, length;
  while (x) {
    num[i] = (x % 10) + '0';
    x /= 10;
    i++;
  }
  length = i;
  for (i = 0; i < length / 2; ++i) {
    char tmp = num[i];
    num[i] = num[length - i - 1];
    num[length - i - 1] = tmp;
  }
}
void interrupt() {  //used for delay functions
  TMR0 = resetTMR0;
  delayCntr++;
  INTCON = INTCON & 0xFB;
}
void moveForward() {
  PORTD = 0xA0;
  Lcd_Out(1, 1, moveForwardTxt);
}
void delayFor_ms(unsigned int d) {
  delayCntr = 0;
  OPTION_REG = 0x87; //256 prescaler
  resetTMR0 = 247;   //overflows every 1ms
  TMR0 = resetTMR0;
  INTCON = INTCON | 0x20; //Enable TMRO Overflow interrupt
  while (delayCntr < d);
  INTCON = INTCON & 0xDF;// Disable TMR0 Overflow interrupt
}
void delayFor_10us(unsigned int d) {
  delayCntr = 0;
  OPTION_REG = 0x80; //2 prescaler
  resetTMR0 = 245;  //overflows every 10us
  TMR0 = resetTMR0;
  INTCON = INTCON | 0x20; //Enable TMRO Overflow interrupt
  while (delayCntr < d);
  INTCON = INTCON & 0xDF;// Disable TMR0 Overflow interrupt
}
unsigned int findDis(int trigPin, int echoPin) {
  int dis;
  delayFor_ms(500);
  PORTC = 0;
  TMR1H = TMR1L = 0;
  delayFor_10us(5);
  PORTC = trigPin;  //start trigger signal 50us pulse
  delayFor_10us(5);
  PORTC = 0x00;
  while ((PORTC & echoPin) == 0);  //wait for echo pulse
  T1CON = 0x01;
  while (PORTC & echoPin);
  T1CON = 0x00;
  dis = (TMR1L | (TMR1H << 8));
  dis = (dis +116 - 1)/ 116;  //dis = ceil(dis/116)
  return dis;
}
unsigned int checkSensor(char type) {
  int dis;
  if (type == FRONTS) {
    dis = findDis(1, 2);
    convertNumToText(dis);
    Lcd_Out(2, 1, frontSensorTxt);
    Lcd_Out(2, 15, num);
    Lcd_Out(2,18,"cm");
  } else if (type == RIGHTS) {
    dis = findDis(4, 8);
  }  else {
    dis = findDis(16, 32);
  }
  if (dis >= thresholdDis)return 0; //no obstacle detected
  return 1;
}
void turn(char type) {
  if (type == RIGHT) {
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Out(1, 1, turnRightTxt);
    PORTD  =  0x80;  //stop right motor
    delayFor_ms(2000);
  } else if (type == LEFT) {
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Out(1, 1, turnLeftTxt);
    PORTD  = 0x20;  //stop left motor
    delayFor_ms(2000);
  } else {
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Out(1, 1, turn180Txt);
    PORTD  =  0x80; //stop right motor, turn 180 degrees
    delayFor_ms(4000);
  }
  Lcd_Cmd(_LCD_CLEAR);
  Lcd_Out(1, 1, doneTurngingTxt);
  delayFor_ms(500);
}
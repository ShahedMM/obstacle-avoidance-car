# README #

This repository represents the design and simulation of an autonomous obstacle avoidance robot car using PIC16F877A microcontroller done as part of the Microprocessors and Embedded Systems Course (Spring 2020).


Unfortunatly due to the current pandemic situation, the hardware part of the project was not completed. The design was tested using Proteus Simulation software. 
The repository includes the documentation of the project.
